﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.Models.Enums
{
    public enum ProductGroupName
    {
        Basic,
        Complex,
        Mold,
    }
}
