﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.Models.CoinsCalculator
{
    public class CoinsOfMagicRewards
    {
        public static List<ItemData> Get()
        {
            var result = new List<ItemData>();

            result.Add(new ItemData()
            {
                Name = "Moonstone Earring",
                Description = "48 crystals C / M.Def: 39",
                Image = "/Content/Images/Coins/MoonstoneEarring.png",
                SilverCount = 0,
                BloodCount = 60,
                GoldCount = 50,
                DefaultValue = 0
            });
            result.Add(new ItemData()
            {
                Name = "Nassen's Earring",
                Description = "130 crystals C / M.Def: 48",
                Image = "/Content/Images/Coins/NassensEarring.png",
                SilverCount = 50,
                BloodCount = 190,
                GoldCount = 50,
                DefaultValue = 0
            });
            result.Add(new ItemData()
            {
                Name = "Ring of Sage",
                Description = "??? crystals ?? / M.Def: 32",
                Image = "/Content/Images/Coins/RingOfSage.jpg",
                SilverCount = 40,
                BloodCount = 40,
                GoldCount = 140,
                DefaultValue = 0
            });
            result.Add(new ItemData()
            {
                Name = "Necklace of Protection",
                Description = "60 crystals C / M.Def: 56",
                Image = "/Content/Images/Coins/NecklaceOfProtection.png",
                SilverCount = 130,
                BloodCount = 30,
                GoldCount = 30,
                DefaultValue = 0
            });
            result.Add(new ItemData()
            {
                Name = "Demon Boots",
                Description = "76 crystals C / P.Def: 36",
                Image = "/Content/Images/Coins/DemonBoots.png",
                SilverCount = 0,
                BloodCount = 0,
                GoldCount = 200,
                DefaultValue = 0
            });
            result.Add(new ItemData()
            {
                Name = "Demon Stockings",
                Description = "142 crystals C / P.Def: 43",
                Image = "/Content/Images/Coins/DemonStockings.png",
                SilverCount = 300,
                BloodCount = 0,
                GoldCount = 0,
                DefaultValue = 0
            });
            result.Add(new ItemData()
            {
                Name = "Demon Gloves",
                Description = "76 crystals C / P.Def: 36",
                Image = "/Content/Images/Coins/DemonGloves.png",
                SilverCount = 0,
                BloodCount = 0,
                GoldCount = 200,
                DefaultValue = 0
            });
            result.Add(new ItemData()
            {
                Name = "Full Plate Helmet",
                Description = "165 crystals C / P.Def: 58",
                Image = "/Content/Images/Coins/FullPlateHelmet.png",
                SilverCount = 0,
                BloodCount = 200,
                GoldCount = 200,
                DefaultValue = 0
            });
            result.Add(new ItemData()
            {
                Name = "Demon Staff",
                Description = "1885 crystals C / P.Atk: 152, M.Atk 111 (top C Weapon)",
                Image = "/Content/Images/Coins/DemonsStaff.png",
                SilverCount = 1000,
                BloodCount = 2000,
                GoldCount = 1300,
                DefaultValue = 21000000
            });
            result.Add(new ItemData()
            {
                Name = "Dark Screamer",
                Description = "1322 crystals C / P.Atk 122, M.Atk: 76",
                Image = "/Content/Images/Coins/DarkScreamer.png",
                SilverCount = 100,
                BloodCount = 1000,
                GoldCount = 2100,
                DefaultValue = 0
            });
            result.Add(new ItemData()
            {
                Name = "Widow Maker",
                Description = "1471 crystals C / P.Atk: 144, M.Atk: 78",
                Image = "/Content/Images/Coins/WidowMaker.png",
                SilverCount = 2000,
                BloodCount = 300,
                GoldCount = 1000,
                DefaultValue = 0
            });
            result.Add(new ItemData()
            {
                Name = "Sword of Limit",
                Description = "1322 crystals C / P.Atk: 139, M.Atk: 76",
                Image = "/Content/Images/Coins/SwordOfLimit.png",
                SilverCount = 1100,
                BloodCount = 1000,
                GoldCount = 1000,
                DefaultValue = 0
            });

            return result;
        }
    }
}
