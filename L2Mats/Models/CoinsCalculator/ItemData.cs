﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.Models.CoinsCalculator
{
    public class ItemData
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }

        public int GoldCount { get; set; }
        public int SilverCount { get; set; }
        public int BloodCount { get; set; }

        public int DefaultValue { get; set; }
    }
}
