﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.Models.CoinsCalculator
{
    public class CoinPrice
    {
        public int GoldPrice { get; set; }
        public int SilverPrice { get; set; }
        public int BloodPrice { get; set; }

        public double GoldRatio { get; set; }
        public double SilverRatio { get; set; }
        public double BloodRatio { get; set; }

        public static CoinPrice GetDefault()
        {
            return new CoinPrice()
            {
                GoldPrice = 5000,
                BloodPrice = 5000,
                SilverPrice = 1000,

                GoldRatio = 5,
                BloodRatio = 5,
                SilverRatio = 1,
            };
        }
    }
}
