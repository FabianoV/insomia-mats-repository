﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.Models
{
    public class PriceLog
    {
        public Guid Id { get; set; }
        public DateTime Time { get; set; }
        public string AddingUser { get; set; }
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public int Price { get; set; }
        public ShopType ShopType { get; set; }
        public string PlayerName { get; set; }
    }
}
