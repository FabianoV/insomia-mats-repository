﻿using L2Mats.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.Models
{
    public class ProductGroup
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string IconUrl { get; set; }
        public ProductGroupName Group { get; set; }

        public List<Product> Products { get; set; }
    }
}
