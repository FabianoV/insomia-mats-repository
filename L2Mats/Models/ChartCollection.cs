﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.Models
{
    public class ChartCollection
    {
        public List<ChartPoint> AvegarePoints { get; set; }
        public List<ChartPoint> MinPoints { get; set; }
        public List<ChartPoint> MaxPoints { get; set; }
    }
}
