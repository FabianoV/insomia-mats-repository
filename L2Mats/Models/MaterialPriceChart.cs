﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace L2Mats.Models
{
    public class MaterialPriceChart
    {
        public ObservableCollection<PriceLog> Logs { get; set; }
        public ObservableCollection<PriceLog> BuyLogs { get; set; }
        public ObservableCollection<PriceLog> SellLogs { get; set; }

        public int MaxPriceOnChart { get; set; }
        public int MinPriceOnChart { get; set; }

    }
}
