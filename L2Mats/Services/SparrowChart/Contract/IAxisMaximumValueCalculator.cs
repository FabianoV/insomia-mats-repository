﻿using L2Mats.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.Services.SparrowChart.Contract
{
    public interface IAxisMaximumValueCalculator
    {
        int CalculateAxisMaximumValue(IEnumerable<ChartPoint> points);
    }
}
