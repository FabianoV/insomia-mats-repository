﻿using L2Mats.Models;
using L2Mats.Services.SparrowChart.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.Services.SparrowChart.Implementation
{
    public class AxisMaximumValueCalculator : IAxisMaximumValueCalculator
    {
        // Methods
        public int CalculateAxisMaximumValue(IEnumerable<ChartPoint> points)
        {
            if (points.Count() == 0)
            {
                return 10;
            }

            var maximumValue = points.Max(p => p.Y);
            maximumValue = (int)(maximumValue * 1.05 + 2);
            return maximumValue;
        }
    }
}
