﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.Services.Others.Contract
{
    public interface IOnNavigatedEventRelease
    {
        void Release(string viewName);
    }
}
