﻿using CommonServiceLocator;
using L2Mats.Pages;
using L2Mats.Services.Others.Contract;
using L2Mats.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.Services.Others.Implementation
{
    public class OnNavigatedEventRelease : IOnNavigatedEventRelease
    {
        //Properties
        public ViewAndViewModelDataCollection Data { get; set; }

        //Constructor
        public OnNavigatedEventRelease()
        {
            Data = new ViewAndViewModelDataCollection();
            Data.Register("LoginPage", typeof(LoginPage), typeof(LoginViewModel));
            Data.Register("AddProductLogPage", typeof(AddProductLogPage), typeof(AddProductLogViewModel));
            Data.Register("ProductDetailsPage", typeof(ProductDetailsPage), typeof(ProductDetailsViewModel));
            Data.Register("ProductsListPage", typeof(ProductsListPage), typeof(ProductListViewModel));
            Data.Register("GoogleRandomImagePage", typeof(GoogleRandomImagePage), typeof(GoogleRandomImageViewModel));
            Data.Register("CoinsCalculatorPage", typeof(CoinsCalculatorPage), typeof(CoinsCalculatorViewModel));
            Data.Register("PriceChartPage", typeof(PriceChartPage), typeof(PriceChartViewModel)); 
            Data.Register("ClanContributionsPage", typeof(ClanContributionsPage), typeof(ClanContributionViewModel));
        }

        //Methods
        public void Release(string viewName)
        {
            Type viewModelType = Data.FindViewModelTypeByName(viewName);
            if (viewModelType != null)
            {
                object instance = ServiceLocator.Current.GetInstance(viewModelType);
                if (instance is IOnNavigatedTo)
                {
                    IOnNavigatedTo instanceChecked = instance as IOnNavigatedTo;
                    instanceChecked.OnNavigatedTo();
                }
            }
        }
    }

    public class ViewAndViewModelDataCollection
    {
        public List<ViewAndViewModelData> Data { get; set; } = new List<ViewAndViewModelData>();

        public void Register(string name, Type view, Type viewModel)
        {
            ViewAndViewModelData vavmd = new ViewAndViewModelData()
            {
                Name = name,
                ViewType = view,
                ViewModelType = viewModel,
            };
            Data.Add(vavmd);
        }
        public Type FindViewTypeByName(string name)
        {
            ViewAndViewModelData data = Data.FirstOrDefault(d => d.Name == name);

            if (data != null)
            {
                return data.ViewType;
            }

            return null;
        }
        public Type FindViewModelTypeByName(string name)
        {
            ViewAndViewModelData data = Data.FirstOrDefault(d => d.Name == name);

            if (data != null)
            {
                return data.ViewModelType;
            }

            return null;
        }
    }

    public class ViewAndViewModelData
    {
        public string Name { get; set; }
        public Type ViewType { get; set; }
        public Type ViewModelType { get; set; }
    }
}
