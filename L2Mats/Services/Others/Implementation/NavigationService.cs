﻿using GalaSoft.MvvmLight.Views;
using L2Mats.Services.Others.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace L2Mats.Services.Others.Implementation
{
    public class NavigationService : INavigationService, INavigationInitializer
    {
        //Services
        public IOnNavigatedEventRelease NavigatedToRelease { get; set; }

        //Properties
        private Frame Frame { get; set; }
        public string CurrentPageKey { get; set; }
        public Dictionary<string, string> PagesKeys { get; set; }
        public List<string> History { get; set; }

        //Constructor
        public NavigationService(IOnNavigatedEventRelease naviagetdToEventRelease)
        {
            //Services
            NavigatedToRelease = naviagetdToEventRelease;

            //Initialize
            PagesKeys = new Dictionary<string, string>() { };
            History = new List<string>();
        }

        //Methods
        public void Initialize(Frame frame)
        {
            Frame = frame;
        }

        public void GoBack()
        {
            string nextPage = History[History.Count - 2];
            History.Add(nextPage);
            Frame.Source = new Uri(nextPage, UriKind.Relative);
        }

        public void NavigateTo(string pageKey)
        {
            NavigateTo(pageKey, null);
        }

        public void NavigateTo(string pageKey, object parameter)
        {
            string nextPage = "MainMenuPage.xaml";

            if (PagesKeys.ContainsKey(pageKey))
            {
                nextPage = PagesKeys[pageKey];
            }
            else
            {
                nextPage = "Pages/" + pageKey + ".xaml";
            }

            History.Add(nextPage);
            Frame.Source = new Uri(nextPage, UriKind.Relative);
            NavigatedToRelease.Release(pageKey);
        }
    }
}
