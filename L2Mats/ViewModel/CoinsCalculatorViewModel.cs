﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using L2Mats.Models.CoinsCalculator;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.ViewModel
{
    public class CoinsCalculatorViewModel : ViewModelBase
    {
        // Services
        public INavigationService NavigationService { get; set; }

        // Properties
        public int ItemPrice { get; set; }
        public string ItemPriceInWords { get; set; }
        public int SelctedItemIndex { get; set; }
        public CoinPrice CurrentCoinPrice { get; set; }
        public ItemData CurrentItem { get; set; }
        public ObservableCollection<ItemData> Items { get; set; }

        // Commands
        public RelayCommand OpenCoinCalculatorPageCommand { get; set; }
        public RelayCommand CalculateItemPriceCommand { get; set; }
        public RelayCommand CalculateCoinsPriceCommand { get; set; }

        // Constructor
        public CoinsCalculatorViewModel()
        {
            // Services
            NavigationService = SimpleIoc.Default.GetInstance<INavigationService>();

            // Commands
            OpenCoinCalculatorPageCommand = new RelayCommand(OpenCoinCalculatorPage);
            CalculateItemPriceCommand = new RelayCommand(CalculateItemPrice);
            CalculateCoinsPriceCommand = new RelayCommand(CalculateCoinsPrice);

            // Init
            Items = new ObservableCollection<ItemData>(CoinsOfMagicRewards.Get());
            CurrentCoinPrice = CoinPrice.GetDefault();
            CurrentItem = Items.First();
        }

        // Methods
        public void OpenCoinCalculatorPage()
        {
            NavigationService.NavigateTo("CoinsCalculatorPage");
        }

        public void RealodItem()
        {
            CurrentItem = Items[SelctedItemIndex];
            RaisePropertyChanged(nameof(CurrentItem));

            ItemPrice = CurrentItem.DefaultValue;
            RaisePropertyChanged(nameof(CurrentItem));
        }

        public void CalculateItemPrice()
        {
            var i = CurrentItem;
            var p = CurrentCoinPrice;

            ItemPrice = p.GoldPrice * i.GoldCount + p.BloodPrice * i.BloodCount + p.SilverPrice * i.SilverCount;
            RaisePropertyChanged(nameof(ItemPrice));
            CreateItemPriceInWords();
        }

        private void CreateItemPriceInWords()
        {
            var price = ItemPrice;
            ItemPriceInWords = "";
            if (price >= 1000000)
            {
                ItemPriceInWords += $"{price / 1000000}kk ";
                price = price % 1000000;
            }
            if (price >= 1000)
            {
                ItemPriceInWords += $"{price / 1000}k ";
                price = price % 1000;
            }
            if (price != 0)
            {
                ItemPriceInWords += $"{price} ";
            }
            ItemPriceInWords += "adena";
            RaisePropertyChanged(nameof(ItemPriceInWords));
        }

        public void CalculateCoinsPrice()
        {
            var p = CurrentCoinPrice;
            var i = CurrentItem;
            var points = p.GoldRatio * i.GoldCount + p.BloodRatio * i.BloodCount + p.SilverRatio * i.SilverCount;
            var adenaPerPoint = ItemPrice / points;

            CurrentCoinPrice.GoldPrice = Convert.ToInt32(adenaPerPoint * p.GoldRatio);
            CurrentCoinPrice.BloodPrice = Convert.ToInt32(adenaPerPoint * p.BloodRatio);
            CurrentCoinPrice.SilverPrice = Convert.ToInt32(adenaPerPoint * p.SilverRatio);

            RaisePropertyChanged(nameof(CurrentCoinPrice));
        }
    }
}
