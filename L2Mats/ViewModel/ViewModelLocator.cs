using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using L2Mats.Repositories;
using L2Mats.Services.Others.Contract;
using L2Mats.Services.Others.Implementation;
using L2Mats.Services.SparrowChart.Contract;
using L2Mats.Services.SparrowChart.Implementation;

namespace L2Mats.ViewModel
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            // View Models
            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<LoginViewModel>();
            SimpleIoc.Default.Register<ProductListViewModel>();
            SimpleIoc.Default.Register<ProductDetailsViewModel>();
            SimpleIoc.Default.Register<AddProductLogViewModel>();
            SimpleIoc.Default.Register<AboutViewModel>();
            SimpleIoc.Default.Register<GoogleRandomImageViewModel>();
            SimpleIoc.Default.Register<CoinsCalculatorViewModel>();
            SimpleIoc.Default.Register<PriceChartViewModel>();
            SimpleIoc.Default.Register<ClanContributionViewModel>();

            // Repositories
            SimpleIoc.Default.Register<ProductRepository>();
            SimpleIoc.Default.Register<ProductGroupRepository>();
            SimpleIoc.Default.Register<PriceLogRepository>();
            SimpleIoc.Default.Register<PriceChartRepository>();
            SimpleIoc.Default.Register<GoogleImageRepository>();
            SimpleIoc.Default.Register<UsersRepository>();

            // Services
            SimpleIoc.Default.Register<INavigationService, NavigationService>();
            SimpleIoc.Default.Register<IOnNavigatedEventRelease, OnNavigatedEventRelease>();
            SimpleIoc.Default.Register<IAxisMaximumValueCalculator, AxisMaximumValueCalculator>();
        }

        public MainViewModel Main { get { return ServiceLocator.Current.GetInstance<MainViewModel>(); } }
        public LoginViewModel Login { get { return ServiceLocator.Current.GetInstance<LoginViewModel>(); } }
        public ProductListViewModel ProductList { get { return ServiceLocator.Current.GetInstance<ProductListViewModel>(); } }
        public ProductDetailsViewModel ProductPriceChart { get { return ServiceLocator.Current.GetInstance<ProductDetailsViewModel>(); } }
        public AddProductLogViewModel AddProductLog { get { return ServiceLocator.Current.GetInstance<AddProductLogViewModel>(); } }
        public AboutViewModel About { get { return ServiceLocator.Current.GetInstance<AboutViewModel>(); } }
        public GoogleRandomImageViewModel GoogleRandomImageViewModel { get { return ServiceLocator.Current.GetInstance<GoogleRandomImageViewModel>(); } }
        public CoinsCalculatorViewModel CoinsCalculatorViewModel { get { return ServiceLocator.Current.GetInstance<CoinsCalculatorViewModel>(); } }
        public PriceChartViewModel PriceChartViewModel { get { return ServiceLocator.Current.GetInstance<PriceChartViewModel>(); } }
        public ClanContributionViewModel ClanContributionViewModel { get { return ServiceLocator.Current.GetInstance<ClanContributionViewModel>(); } }

        public static void Cleanup()
        {

        }
    }
}