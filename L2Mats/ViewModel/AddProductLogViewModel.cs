﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using L2Mats.Models;
using L2Mats.Models.Enums;
using L2Mats.Repositories;
using L2Mats.Services.Others.Contract;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.ViewModel
{
    public class AddProductLogViewModel : ViewModelBase, IOnNavigatedTo
    {
        // Fields
        private bool isCustomDateUsed;

        // Services
        public INavigationService NavigationService { get; set; }

        // Repositories
        public PriceLogRepository PriceLogRepository { get; set; }
        public ProductRepository ProductRepository { get; set; }

        // Commands
        public RelayCommand LogPriceCommand { get; set; }
        public RelayCommand BackToListCommand { get; set; }

        // Properties
        public string CurrentProductName { get; set; }
        public Product CurrentProduct { get; set; }
        public int CurrentPrice { get; set; }
        public string CurrentShopOwner { get; set; }
        public ObservableCollection<PriceLog> PriceLogList { get; set; }
        public bool IsCustomDateUsed { get { return isCustomDateUsed; } set { isCustomDateUsed = value; RaisePropertyChanged(nameof(IsCustomDateUsed)); } }
        public DateTime SelectedDate { get; set; }

        // Constructor
        public AddProductLogViewModel()
        {
            // Services
            NavigationService = SimpleIoc.Default.GetInstance<INavigationService>();

            // Repositories
            PriceLogRepository = SimpleIoc.Default.GetInstance<PriceLogRepository>();
            ProductRepository = SimpleIoc.Default.GetInstance<ProductRepository>();

            // Commands
            LogPriceCommand = new RelayCommand(LogPrice);
            BackToListCommand = new RelayCommand(BackToList);

            // Init
            PriceLogList = new ObservableCollection<PriceLog>();
        }

        // Methods
        public void LogPrice()
        {
            var priceLog = new PriceLog()
            {
                ProductId = CurrentProduct.Id,
                ProductName = CurrentProduct.Name,
                Price = CurrentPrice,
                Time = IsCustomDateUsed ? SelectedDate : DateTime.Now,
                ShopType = ShopType.Sell,
                PlayerName = CurrentShopOwner,
                AddingUser = SimpleIoc.Default.GetInstance<LoginViewModel>().CurrentLogin,
            };

            PriceLogRepository.Save(priceLog);
            RefreshPriceLogList();
        }

        public void OnNavigatedTo()
        {
            CurrentProduct = ProductRepository.GetProductByName(CurrentProductName).Result;
            RefreshPriceLogList();
            RaisePropertyChanged(nameof(CurrentProduct));
        }

        private void RefreshPriceLogList()
        {
            PriceLogList.Clear();
            var priceLogs = PriceLogRepository.Get(CurrentProductName);
            priceLogs.ForEach(pl => PriceLogList.Add(pl));
        }

        private void BackToList()
        {
            NavigationService.GoBack();
        }
    }
}
