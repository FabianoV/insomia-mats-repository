﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.ViewModel
{
    public class PriceChartViewModel : ViewModelBase
    {
        // Services
        public INavigationService NavigationService { get; set; }

        // Commands
        public RelayCommand OpenPriceChartPageCommand { get; set; }

        // Constructor
        public PriceChartViewModel()
        {
            // Services
            NavigationService = SimpleIoc.Default.GetInstance<INavigationService>();

            // Commands
            OpenPriceChartPageCommand = new RelayCommand(OpenPriceChartPage);
        }

        // Methods
        public void OpenPriceChartPage()
        {
            NavigationService.NavigateTo("PriceChartPage");
        }
    }
}
