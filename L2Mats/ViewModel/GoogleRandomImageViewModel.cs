﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using L2Mats.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.ViewModel
{
    public class GoogleRandomImageViewModel : ViewModelBase
    {
        // Services
        public INavigationService NavigationService { get; set; }

        // Repositories
        public GoogleImageRepository GoogleImageRepository { get; set; }

        // Properties
        public string CurrentUrl { get; set; }
        public string Topic { get; set; }

        // Commands
        public RelayCommand GetImageCommand { get; set; }
        public RelayCommand OpenGoogleImagePageCommand { get; set; }

        // Constructor
        public GoogleRandomImageViewModel()
        {
            GoogleImageRepository = SimpleIoc.Default.GetInstance<GoogleImageRepository>();
            NavigationService = SimpleIoc.Default.GetInstance<INavigationService>();
            GetImageCommand = new RelayCommand(GetImage);
            OpenGoogleImagePageCommand = new RelayCommand(OpenGoogleImagePage);
        }

        // Methods
        public void OpenGoogleImagePage()
        {
            NavigationService.NavigateTo("GoogleRandomImagePage");
        }

        public void GetImage()
        {
            CurrentUrl = GoogleImageRepository.GetImage(Topic);
            RaisePropertyChanged(nameof(CurrentUrl));
        }
    }
}
