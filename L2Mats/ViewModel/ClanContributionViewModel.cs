﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.ViewModel
{
    public class ClanContributionViewModel : ViewModelBase
    {
        // Services
        public INavigationService NavigationService { get; set; }

        // Commands
        public RelayCommand OpenClanContributionsPageCommand { get; set; }

        // Constructor
        public ClanContributionViewModel()
        {
            // Services
            NavigationService = SimpleIoc.Default.GetInstance<INavigationService>();

            // Commands
            OpenClanContributionsPageCommand = new RelayCommand(OpenClanContributionsPage);
        }

        // Methods
        private void OpenClanContributionsPage()
        {
            NavigationService.NavigateTo("ClanContributionsPage");
        }
    }
}
