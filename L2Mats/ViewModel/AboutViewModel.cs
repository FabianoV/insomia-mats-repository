﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.ViewModel
{
    public class AboutViewModel : ViewModelBase
    {
        // Service
        public INavigationService NavigationService { get; set; }

        // Commands
        public RelayCommand OpenAboutPageCommand { get; set; }
        public RelayCommand BackCommand { get; set; }

        // Constructor
        public AboutViewModel()
        {
            // Services
            NavigationService = SimpleIoc.Default.GetInstance<INavigationService>();

            // Command
            OpenAboutPageCommand = new RelayCommand(OpenAboutPage);
            BackCommand = new RelayCommand(Back);
        }

        // Methods
        private void OpenAboutPage()
        {
            NavigationService.NavigateTo("AboutPage");
        }

        private void Back()
        {
            NavigationService.GoBack();
        }
    }
}
