﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using L2Mats.DesignPrototypes;
using L2Mats.Models;
using L2Mats.Repositories;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace L2Mats.ViewModel
{
    public class ProductListViewModel : ViewModelBase
    {
        // Services
        public INavigationService NavigationService { get; set; }

        // Repositories
        public ProductRepository ProductRepository { get; set; }
        public ProductGroupRepository ProductGroupRepository { get; set; }
        public PriceLogRepository PriceLogRepository { get; set; }

        // Properties
        public ObservableCollection<ProductGroup> Groups { get; set; }
        public ObservableCollection<Product> Products { get; set; }

        // Add product Properties
        public ProductGroup SelectedGroup { get; set; }
        public Product CurrentProduct { get; set; }
        public int CurrentPrice { get; set; }
        public string CurrentShopOwner { get; set; }
        public ObservableCollection<PriceLog> CurrentMaterialLog { get; set; }

        // Commands
        public RelayCommand OpenProductListPageCommand { get; set; }
        public RelayCommand<string> AddPriceCommand { get; set; }
        public RelayCommand<string> OpenDetailsPageCommand { get; set; }

        // Constructor
        public ProductListViewModel()
        {
            NavigationService = SimpleIoc.Default.GetInstance<INavigationService>(); 

            ProductRepository = SimpleIoc.Default.GetInstance<ProductRepository>();
            ProductGroupRepository = SimpleIoc.Default.GetInstance<ProductGroupRepository>();
            PriceLogRepository = SimpleIoc.Default.GetInstance<PriceLogRepository>();

            Groups = new ObservableCollection<ProductGroup>(ProductGroupRepository.GetAll());
            Products = new ObservableCollection<Product>(Groups.First().Products);
            CurrentProduct = Products.First();
            CurrentMaterialLog = new ObservableCollection<PriceLog>();

            // Commands
            OpenProductListPageCommand = new RelayCommand(OpenProductListPage);
            AddPriceCommand = new RelayCommand<string>(AddPrice);
            OpenDetailsPageCommand = new RelayCommand<string>(OpenDetailsPage);
        }

        // Methods
        public void OpenProductListPage()
        {
            NavigationService.NavigateTo("ProductsListPage");
        }

        public void AddPrice(string materialName)
        {
            var addProductLogPageViewModel = SimpleIoc.Default.GetInstance<AddProductLogViewModel>();
            addProductLogPageViewModel.CurrentProductName = materialName;
            NavigationService.NavigateTo("AddProductLogPage");
        }

        public void ReloadProductsForGroup()
        {
            Products.Clear();
            SelectedGroup.Products.ForEach(p => Products.Add(p));
            RaisePropertyChanged(nameof(Products));
        }

        private void OpenDetailsPage(string materialName)
        {
            var productPriceChartViewModel = SimpleIoc.Default.GetInstance<ProductDetailsViewModel>();
            productPriceChartViewModel.CurrentProductName = materialName;
            NavigationService.NavigateTo("ProductDetailsPage");
        }
    }
}
