﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using L2Mats.Models;
using L2Mats.Repositories;
using L2Mats.Services.Others.Contract;
using L2Mats.Services.SparrowChart.Contract;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace L2Mats.ViewModel
{
    public class ProductDetailsViewModel : ViewModelBase, IOnNavigatedTo
    {
        // Services
        public IAxisMaximumValueCalculator AxisMaximumValueCalculator { get; set; }
        public INavigationService NavigationService { get; set; }

        // Repositories
        public ProductRepository ProductRepository { get; set; }
        public PriceLogRepository PriceLogRepository { get; set; }
        public PriceChartRepository PriceChartRepository { get; set; }

        // Proeprties
        public string CurrentProductName { get; set; }
        public Product CurrentProduct { get; set; }
        public MaterialPriceChart ChartData { get; set; }
        public PriceLog SelectedPriceLog { get; set; }

        // Avegare Chart Data
        public ObservableCollection<ChartPoint> LogsAvgData { get; set; }
        public int LogsAvgXMaximumChartValue { get; set; }

        // Commands
        public RelayCommand BackToProductListCommand { get; set; }
        public RelayCommand OpenAddPriceCommand { get; set; }
        public RelayCommand RemovePriceCommand { get; set; }

        // Constructor
        public ProductDetailsViewModel()
        {
            // Services
            AxisMaximumValueCalculator = SimpleIoc.Default.GetInstance<IAxisMaximumValueCalculator>();
            NavigationService = SimpleIoc.Default.GetInstance<INavigationService>();

            // Repositories
            ProductRepository = SimpleIoc.Default.GetInstance<ProductRepository>();
            PriceLogRepository = SimpleIoc.Default.GetInstance<PriceLogRepository>();
            PriceChartRepository = SimpleIoc.Default.GetInstance<PriceChartRepository>();

            ChartData = new MaterialPriceChart()
            {
                Logs = new ObservableCollection<PriceLog>(),
                BuyLogs = new ObservableCollection<PriceLog>(),
                SellLogs = new ObservableCollection<PriceLog>(),
                MaxPriceOnChart = 100000,
                MinPriceOnChart = 0,
            };

            LogsAvgData = new ObservableCollection<ChartPoint>();
            foreach (var log in ChartData.Logs)
            {
                var avgPoint = new ChartPoint()
                {
                    X = log.Time,
                    Y = log.Price,
                };
                LogsAvgData.Add(avgPoint);
            }
            LogsAvgXMaximumChartValue = 10000; // AxisMaximumValueCalculator.CalculateAxisMaximumValue(LogsAvgData);

            // Command
            BackToProductListCommand = new RelayCommand(BackToProductList);
            OpenAddPriceCommand = new RelayCommand(OpenAddPrice);
            RemovePriceCommand = new RelayCommand(RemovePrice);
        }

        // Methods
        public void OnNavigatedTo()
        {
            RefreshData();
        }

        private void BackToProductList()
        {
            NavigationService.NavigateTo("ProductsListPage");
        }

        public void OpenAddPrice()
        {
            var addProductLogPageViewModel = SimpleIoc.Default.GetInstance<AddProductLogViewModel>();
            addProductLogPageViewModel.CurrentProductName = CurrentProductName;
            NavigationService.NavigateTo("AddProductLogPage");
        }

        public void RefreshData()
        {
            CurrentProduct = ProductRepository.GetProductByName(CurrentProductName).Result;
            RaisePropertyChanged(nameof(CurrentProduct));

            var logs = PriceLogRepository.Get(CurrentProductName);
            ChartData.Logs.Clear();
            logs.ForEach(l => ChartData.Logs.Add(l));
            ChartData.SellLogs.Clear();
            logs.Where(l => l.ShopType == ShopType.Sell).ToList().ForEach(l => ChartData.SellLogs.Add(l));
            ChartData.BuyLogs.Clear();
            logs.Where(l => l.ShopType == ShopType.Buy).ToList().ForEach(l => ChartData.BuyLogs.Add(l));

            LogsAvgData.Clear();
            var avgLogs = PriceChartRepository.GetPriceProductChart(CurrentProductName, DateTime.Now.AddDays(-30), DateTime.Now).AvegarePoints;
            avgLogs.ForEach(l => LogsAvgData.Add(l));

            RaisePropertyChanged(nameof(ChartData));
        }

        public void RemovePrice()
        {
            PriceLogRepository.Remove(SelectedPriceLog);
            RefreshData();
        }
    }
}
