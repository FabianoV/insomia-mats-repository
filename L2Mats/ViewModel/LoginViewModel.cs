﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using L2Mats.Models;
using L2Mats.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace L2Mats.ViewModel
{
    public class LoginViewModel : ViewModelBase
    {
        // Services
        public INavigationService Navigation { get; set; }
        public UsersRepository UsersRepository { get; set; }

        // Properties
        public bool IsLoggedIn { get; set; }
        public string IsLoggedInMessage { get; set; }
        public string CurrentLogin { get; set; } = "Admin";
        public List<User> Users { get; set; }

        // Commands
        public RelayCommand<PasswordBox> LoginCommand { get; set; }
        public RelayCommand LogoutCommand { get; set; }

        //Constructor
        public LoginViewModel()
        {
            Navigation = SimpleIoc.Default.GetInstance<INavigationService>();
            UsersRepository = SimpleIoc.Default.GetInstance<UsersRepository>();

            IsLoggedIn = false;
            IsLoggedInMessage = "User is not logged in.";
            Users = new List<User>();

            Users.Add(new User()
            {
                Username = "Admin",
                Password = "Admin"
            });

            LoginCommand = new RelayCommand<PasswordBox>(Login);
            LogoutCommand = new RelayCommand(Logout);
        }

        // Methods
        public void Login(PasswordBox passwordControl)
        {
            // var user = Users.FirstOrDefault(u => u.Username == CurrentLogin && u.Password == passwordControl.Password);
            var user = UsersRepository.Authenticate(CurrentLogin, passwordControl.Password); 

            if (user != null)
            {
                IsLoggedInMessage = $"Logged as: {user.Username}";
                IsLoggedIn = true;
                RaisePropertyChanged(nameof(CurrentLogin));
                RaisePropertyChanged(nameof(IsLoggedInMessage));
                RaisePropertyChanged(nameof(IsLoggedIn));

                Navigation.NavigateTo("ProductsListPage");
            }
        }

        public void Logout()
        {
            IsLoggedInMessage = "User is not logged in.";
            IsLoggedIn = false;
            RaisePropertyChanged(nameof(CurrentLogin));
            RaisePropertyChanged(nameof(IsLoggedInMessage));
            RaisePropertyChanged(nameof(IsLoggedIn));

            Navigation.NavigateTo("LoginPage");
        }
    }
}
