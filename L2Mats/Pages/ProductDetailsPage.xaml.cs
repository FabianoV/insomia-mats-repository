﻿using GalaSoft.MvvmLight.Ioc;
using L2Mats.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace L2Mats.Pages
{
    /// <summary>
    /// Interaction logic for ProductDetailsPage.xaml
    /// </summary>
    public partial class ProductDetailsPage : Page
    {
        public ProductDetailsPage()
        {
            InitializeComponent();
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            var productDetailsViewModel = SimpleIoc.Default.GetInstance<ProductDetailsViewModel>();
            productDetailsViewModel.RefreshData();

            CustomPriceChart.ProductName = productDetailsViewModel.CurrentProductName;
            CustomPriceChart.Refresh();
        }
    }
}
