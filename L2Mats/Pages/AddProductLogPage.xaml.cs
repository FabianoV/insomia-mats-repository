﻿using GalaSoft.MvvmLight.Ioc;
using L2Mats.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace L2Mats.Pages
{
    /// <summary>
    /// Interaction logic for AddProductLogPage.xaml
    /// </summary>
    public partial class AddProductLogPage : Page
    {
        public AddProductLogPage()
        {
            InitializeComponent();
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void DateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            var dtp = sender as DateTimePicker;
            var dataContext = SimpleIoc.Default.GetInstance<AddProductLogViewModel>();
            dataContext.SelectedDate = dtp.Value;
        }
    }
}
