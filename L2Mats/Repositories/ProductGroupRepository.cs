﻿using L2Mats.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.Repositories
{
    public class ProductGroupRepository
    {
        // Methods
        public List<ProductGroup> GetAll()
        {
            var serverUrl = ConfigurationManager.AppSettings["L2MatsServerUrl"].ToString();
            var methodUrl = $"{serverUrl}/ProductsGroup/GetAll";
            var productsJson = "";

            using (var client = new HttpClient())
            {
                productsJson = client.GetStringAsync(methodUrl).Result;
            }

            var materials = JsonConvert.DeserializeObject<List<ProductGroup>>(productsJson);
            return materials;
        }
    }
}
