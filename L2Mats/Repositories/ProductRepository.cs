﻿using L2Mats.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.Repositories
{
    public class ProductRepository
    {
        // Methods
        public List<Product> GetAll()
        {
            var serverUrl = ConfigurationManager.AppSettings["L2MatsServerUrl"].ToString();
            var methodUrl = $"{serverUrl}/Products/GetAll";
            var productsJson = "";

            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromSeconds(5);
                productsJson = client.GetStringAsync(methodUrl).Result;
            }

            var materials = JsonConvert.DeserializeObject<List<Product>>(productsJson);
            return materials;
        }

        public List<Product> GetByGroup()
        {
            var serverUrl = ConfigurationManager.AppSettings["L2MatsServerUrl"].ToString();
            var methodUrl = $"{serverUrl}/Products/GetByGroup";
            var productsJson = "";

            using (var client = new HttpClient())
            {
                productsJson = client.GetStringAsync(methodUrl).Result;
            }

            var materials = JsonConvert.DeserializeObject<List<Product>>(productsJson);
            return materials;
        }

        public async Task<Product> GetProductByName(string name)
        {
            return GetAll().FirstOrDefault(m => m.Name == name);
        }
    }
}
