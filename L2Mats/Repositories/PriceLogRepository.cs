﻿using L2Mats.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.Repositories
{
    public class PriceLogRepository
    {
        // Methods
        public List<PriceLog> Get(string productName)
        {
            var serverUrl = ConfigurationManager.AppSettings["L2MatsServerUrl"].ToString();
            var methodUrl = $"{serverUrl}/PriceLog/Get?productName={productName}";
            var logsJson = "";

            using (var client = new HttpClient())
            {
                logsJson = client.GetStringAsync(methodUrl).Result;
            }

            var materials = JsonConvert.DeserializeObject<List<PriceLog>>(logsJson);
            return materials;
        }

        public void Save(PriceLog priceLog)
        {
            var serverUrl = ConfigurationManager.AppSettings["L2MatsServerUrl"].ToString();
            var methodUrl = $"{serverUrl}/PriceLog/Add";
            var content = new StringContent(JsonConvert.SerializeObject(priceLog), Encoding.UTF8, "application/json");

            using (var client = new HttpClient())
            {
                var response = client.PostAsync(methodUrl, content).Result;

                if (!response.IsSuccessStatusCode)
                {
                    var message = response.Content.ReadAsStringAsync().Result;
                    throw new Exception(message);
                }
            }
        }

        public void Remove(PriceLog priceLog)
        {
            var serverUrl = ConfigurationManager.AppSettings["L2MatsServerUrl"].ToString();
            var methodUrl = $"{serverUrl}/PriceLog/Remove?logId={priceLog.Id}";

            using (var client = new HttpClient())
            {
                var response = client.DeleteAsync(methodUrl).Result;

                if (!response.IsSuccessStatusCode)
                {
                    var message = response.Content.ReadAsStringAsync().Result;
                    throw new Exception(message);
                }
            }
        }
    }
}
