﻿using L2Mats.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.Repositories
{
    public class PriceChartRepository
    {
        // Properties
        public List<PriceLog> PriceLogList { get; set; }

        // Constructor
        public PriceChartRepository()
        {
            PriceLogList = new List<PriceLog>();
        }

        // Methods
        public ChartCollection GetPriceProductChart(string productName, DateTime fromDate, DateTime toDate)
        {
            var result = new ChartCollection();
            var serverUrl = ConfigurationManager.AppSettings["L2MatsServerUrl"].ToString();
            var methodUrl = $"{serverUrl}/PriceProductChart/GetLastMonthData?productName={productName}&fromDate={fromDate}&toDate={toDate}";

            using (var client = new HttpClient())
            {
                var response = client.GetAsync(methodUrl).Result;

                if (!response.IsSuccessStatusCode)
                {
                    var message = response.Content.ReadAsStringAsync().Result;
                    throw new Exception(message);
                }

                var json = response.Content.ReadAsStringAsync().Result;
                result = JsonConvert.DeserializeObject<ChartCollection>(json);
            }

            return result;
        }
    }
}
