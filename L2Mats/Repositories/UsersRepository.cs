﻿using L2Mats.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.Repositories
{
    public class UsersRepository
    {
        // Constructor
        public UsersRepository()
        {

        }

        // Methods
        public void Register()
        {

        }

        public User Authenticate(string login, string password)
        {
            var serverUrl = ConfigurationManager.AppSettings["L2MatsServerUrl"].ToString();
            var methodUrl = $"{serverUrl}/Users/authenticate";
            var body = "{\"username\": \"" + login + "\",\"password\": \"" + password + "\"}";
            HttpResponseMessage response = null;

            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromSeconds(5);
                try
                {
                    response = client.PostAsync(methodUrl, new StringContent(body, Encoding.UTF8, "application/json")).Result;
                }
                catch (Exception e)
                {

                }
            }

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var user = JsonConvert.DeserializeObject<User>(response.Content.ReadAsStringAsync().Result);
            return user;
        }
    }
}
