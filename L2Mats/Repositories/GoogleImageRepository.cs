﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace L2Mats.Repositories
{
    public class GoogleImageRepository
    {
        // Properties
        public Random Random { get; set; }

        // Constructor
        public GoogleImageRepository()
        {
            Random = new Random();
        }

        // Methods
        public string GetImage(string topic)
        {
            var html = GetHtmlCode(topic);
            var urls = GetUrls(html);
            return urls[Random.Next(0, urls.Count - 1)];
        }

        private List<string> GetUrls(string html)
        {
            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(html);
            var nodes = document.DocumentNode.SelectNodes(".//img");
            return nodes.Select(n => n?.Attributes["src"].Value).ToList();
        }

        private string GetHtmlCode(string topic)
        {
            string url = $"https://www.google.com/search?q={topic}&tbm=isch";
            string data = "";

            var request = (HttpWebRequest)WebRequest.Create(url);
            var response = (HttpWebResponse)request.GetResponse();

            using (Stream dataStream = response.GetResponseStream())
            {
                if (dataStream == null)
                    return "";
                using (var sr = new StreamReader(dataStream))
                {
                    data = sr.ReadToEnd();
                }
            }
            return data;
        }
    }
}
