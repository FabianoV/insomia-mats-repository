﻿using L2Mats.Models;
using L2Mats.Models.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2Mats.DesignPrototypes
{
    public class ProductGroupListDesignPrototype : ObservableCollection<ProductGroup>
    {
        // Constructor
        public ProductGroupListDesignPrototype()
        {
            var group1 = new ProductGroup()
            {
                Id = Guid.NewGuid(),
                Name = "Basic",
                Group = ProductGroupName.Basic,
                IconUrl = "https://img.icons8.com/search",
                Products = new List<Product>()
                {
                    new Product()
                    {
                        Id = Guid.NewGuid(),
                        Name = "Product 1",
                        Image = "https://img.icons8.com/search",
                        Group = ProductGroupName.Basic,
                        LocalImage = "https://img.icons8.com/search"
                    },
                    new Product()
                    {
                        Id = Guid.NewGuid(),
                        Name = "Product 2",
                        Image = "https://img.icons8.com/search",
                        Group = ProductGroupName.Basic,
                        LocalImage = "https://img.icons8.com/search"
                    },
                }
            };
            Add(group1);

            var group2 = new ProductGroup()
            {
                Id = Guid.NewGuid(),
                Name = "Complex",
                Group = ProductGroupName.Complex,
                IconUrl = "https://img.icons8.com/search",
                Products = new List<Product>()
                {
                    new Product()
                    {
                        Id = Guid.NewGuid(),
                        Name = "Product 1",
                        Image = "https://img.icons8.com/search",
                        Group = ProductGroupName.Basic,
                        LocalImage = "https://img.icons8.com/search"
                    },
                }
            };
            Add(group2);
        }
    }
}
