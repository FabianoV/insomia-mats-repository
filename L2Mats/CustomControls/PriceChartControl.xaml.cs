﻿using GalaSoft.MvvmLight.Ioc;
using L2Mats.Models;
using L2Mats.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace L2Mats.CustomControls
{
    public partial class PriceChartControl : UserControl
    {
        // Const
        const int POINT_WIDTH = 8;
        const int POINT_HEIGHT = 8; 

        // Services
        public PriceChartRepository PriceChartRepository { get; set; }

        // Properties
        public string Mode { get; set; }
        public string ProductName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int MaxYAxisValue { get; set; }
        public int MinYAxisValue { get; set; }
        public List<ChartPoint> MaxValues { get; set; }
        public List<ChartPoint> MinValues { get; set; }
        public List<ChartPoint> AvegareValues { get; set; }

        // Constructor
        public PriceChartControl()
        {
            InitializeComponent();

            PriceChartRepository = SimpleIoc.Default.GetInstance<PriceChartRepository>();

            MaxYAxisValue = 2000;
            MinYAxisValue = 0;
            AvegareValues = new List<ChartPoint>();
            MaxValues = new List<ChartPoint>();
            MinValues = new List<ChartPoint>();

            SwitchTo30DaysModeButton_Click(null, new RoutedEventArgs());
        }

        private void GenerateXAxisLabelsFor60Days()
        {
            ValuesHelpLinesGrid.ColumnDefinitions.Clear();
            XLabelsGrid.ColumnDefinitions.Clear();
            XLabelsGrid.Children.Clear();
            for (int i = 0; i < 60; i++)
            {
                var date = FromDate.AddDays(i);

                ValuesHelpLinesGrid.ColumnDefinitions.Add(new ColumnDefinition());
                XLabelsGrid.ColumnDefinitions.Add(new ColumnDefinition());
                var label = new TextBlock()
                {
                    Text = $"{date.Day}.{date.Month}",
                    HorizontalAlignment = HorizontalAlignment.Center,
                    // RenderTransform = new RotateTransform(90),
                };
                XLabelsGrid.Children.Add(label);
                Grid.SetColumn(label, i);
            }
        }

        private void GenerateXAxisLabelsFor30Days()
        {
            ValuesHelpLinesGrid.ColumnDefinitions.Clear();
            XLabelsGrid.ColumnDefinitions.Clear();
            XLabelsGrid.Children.Clear();
            for (int i = 0; i < 30; i++)
            {
                var date = FromDate.AddDays(i);

                ValuesHelpLinesGrid.ColumnDefinitions.Add(new ColumnDefinition());
                XLabelsGrid.ColumnDefinitions.Add(new ColumnDefinition());
                var label = new TextBlock()
                {
                    Text = $"{date.Day}.{date.Month}",
                    HorizontalAlignment = HorizontalAlignment.Center,
                    // RenderTransform = new RotateTransform(90),
                };
                XLabelsGrid.Children.Add(label);
                Grid.SetColumn(label, i);
            }
        }

        private void GenerateXAxisLabelsFor7Days()
        {
            ValuesHelpLinesGrid.ColumnDefinitions.Clear();
            XLabelsGrid.ColumnDefinitions.Clear();
            XLabelsGrid.Children.Clear();
            for (int i = 0; i < 7; i++)
            {
                var date = FromDate.AddDays(i);

                ValuesHelpLinesGrid.ColumnDefinitions.Add(new ColumnDefinition());
                XLabelsGrid.ColumnDefinitions.Add(new ColumnDefinition());
                var label = new TextBlock()
                {
                    Text = $"{date.Day}.{date.Month}",
                    HorizontalAlignment = HorizontalAlignment.Center,
                    // RenderTransform = new RotateTransform(90),
                };
                XLabelsGrid.Children.Add(label);
                Grid.SetColumn(label, i);
            }
        }

        private void GenerateXAxisLabelsFor1Day()
        {
            ValuesHelpLinesGrid.ColumnDefinitions.Clear();
            XLabelsGrid.ColumnDefinitions.Clear();
            XLabelsGrid.Children.Clear();
            for (int i = 0; i < 24; i++)
            {
                var date = FromDate.AddHours(i);

                ValuesHelpLinesGrid.ColumnDefinitions.Add(new ColumnDefinition());
                XLabelsGrid.ColumnDefinitions.Add(new ColumnDefinition());
                var label = new TextBlock()
                {
                    Text = $"{date.Hour}:00",
                    HorizontalAlignment = HorizontalAlignment.Left,
                    // RenderTransform = new RotateTransform(90),
                };
                XLabelsGrid.Children.Add(label);
                Grid.SetColumn(label, i);
            }
        }

        private void GenerateYAxisLabels()
        {
            ValuesHelpLinesGrid.RowDefinitions.Clear();
            YLabelsGrid.RowDefinitions.Clear();
            YLabelsGrid.Children.Clear();
            var valueDelta = (MaxYAxisValue - MinYAxisValue) / 10;
            var currentValue = MaxYAxisValue;
            for (int i = 0; i < 10; i++)
            {
                ValuesHelpLinesGrid.RowDefinitions.Add(new RowDefinition());
                YLabelsGrid.RowDefinitions.Add(new RowDefinition());
                var label = new TextBlock()
                {
                    Text = $"{currentValue}",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };
                YLabelsGrid.Children.Add(label);
                Grid.SetRow(label, i);
                currentValue -= valueDelta;
            }
        }

        private void CreateDateRangeName()
        {
            DateRangeTextBlock.Text = $"From {FromDate.ToShortDateString()} to {ToDate.ToShortDateString()}";
        }

        public void CalculatePointPosition()
        {
            ValuesCanvas.Children.Clear();

            var pointWidth = POINT_WIDTH;
            var pointHeight = POINT_HEIGHT;

            var canvasWidth = ValuesCanvas.ActualWidth;
            var canvasHeight = ValuesCanvas.ActualHeight;

            if (canvasWidth == 0 || canvasHeight == 0)
            {
                return;
            }

            foreach (var value in AvegareValues)
            {
                if (value.Y > MaxYAxisValue || value.Y < 0)
                {
                    continue;
                }

                var y = ((double)value.Y / (double)MaxYAxisValue) * canvasHeight - pointHeight / 2;

                if (value.X < FromDate || value.X > ToDate)
                {
                    continue;
                }

                var minutesRange = (ToDate - FromDate).TotalMinutes;
                var minutesValue = (ToDate - value.X).TotalMinutes;
                var x = (minutesValue / minutesRange) * canvasWidth - pointWidth / 2;

                Border border = new Border()
                {
                    Width = pointWidth,
                    Height = pointHeight,
                    CornerRadius = new CornerRadius(20),
                };

                Button button = new Button()
                {
                    Background = Brushes.Black,
                    ToolTip = $"Avegare value for day.\n\nPrice: {value.X} adena, \nDate: {value.Y} \n\nClick on point to open more details about this offer.",
                    DataContext = value,
                };

                button.Click += ChartPointButton_Click;
                border.Child = button;
                ValuesCanvas.Children.Add(border);
                Canvas.SetRight(border, x);
                Canvas.SetBottom(border, y);
                Canvas.SetZIndex(border, 3);
            }

            foreach (var value in MinValues)
            {
                if (value.Y > MaxYAxisValue || value.Y < 0)
                {
                    continue;
                }

                var y = ((double)value.Y / (double)MaxYAxisValue) * canvasHeight - pointHeight / 2;

                if (value.X < FromDate || value.X > ToDate)
                {
                    continue;
                }

                var minutesRange = (ToDate - FromDate).TotalMinutes;
                var minutesValue = (ToDate - value.X).TotalMinutes;
                var x = (minutesValue / minutesRange) * canvasWidth - pointWidth / 2;

                Border border = new Border()
                {
                    Width = pointWidth,
                    Height = pointHeight,
                    CornerRadius = new CornerRadius(20),
                };

                Button button = new Button()
                {
                    Background = Brushes.Blue,
                    ToolTip = $"Minimum value for day.\n\nPrice: {value.X} adena, \nDate: {value.Y} \n\nClick on point to open more details about this offer.",
                    DataContext = value,
                };

                button.Click += ChartPointButton_Click;
                border.Child = button;
                ValuesCanvas.Children.Add(border);
                Canvas.SetRight(border, x);
                Canvas.SetBottom(border, y);
                Canvas.SetZIndex(border, 5);
            }

            foreach (var value in MaxValues)
            {
                if (value.Y > MaxYAxisValue || value.Y < 0)
                {
                    continue;
                }

                var y = ((double)value.Y / (double)MaxYAxisValue) * canvasHeight - pointHeight / 2;

                if (value.X < FromDate || value.X > ToDate)
                {
                    continue;
                }

                var minutesRange = (ToDate - FromDate).TotalMinutes;
                var minutesValue = (ToDate - value.X).TotalMinutes;
                var x = (minutesValue / minutesRange) * canvasWidth - pointWidth / 2;

                Border border = new Border()
                {
                    Width = pointWidth,
                    Height = pointHeight,
                    CornerRadius = new CornerRadius(20),
                };

                Button button = new Button()
                {
                    Background = Brushes.Red,
                    ToolTip = $"Maximum value for day.\n\nPrice: {value.X} adena, \nDate: {value.Y} \n\nClick on point to open more details about this offer.",
                    DataContext = value,
                };

                button.Click += ChartPointButton_Click;
                border.Child = button;
                ValuesCanvas.Children.Add(border);
                Canvas.SetRight(border, x);
                Canvas.SetBottom(border, y);
                Canvas.SetZIndex(border, 4);
            }

            GenerateLinesForAvegareValues();
            GenerateLinesForMinValues();
            GenerateLinesForMaxValues();
        }

        private void GenerateLinesForMinValues()
        {
            for (int i = 1; i < MinValues.Count; i++)
            {
                var previousPoint = MinValues[i - 1];
                var currentPoint = MinValues[i];

                var previousPointY = ((double)previousPoint.Y / (double)MaxYAxisValue) * ValuesCanvas.ActualHeight;
                var currentPointY = ((double)currentPoint.Y / (double)MaxYAxisValue) * ValuesCanvas.ActualHeight;
                var minutesRangePreviousPoint = (ToDate - FromDate).TotalMinutes;
                var minutesValuePreviousPoint = (ToDate - previousPoint.X).TotalMinutes;
                var previousPointX = (minutesValuePreviousPoint / minutesRangePreviousPoint) * ValuesCanvas.ActualWidth;
                var minutesRangeCurrentPoint = (ToDate - FromDate).TotalMinutes;
                var minutesValueCurrentPoint = (ToDate - currentPoint.X).TotalMinutes;
                var currentPointX = (minutesValueCurrentPoint / minutesRangeCurrentPoint) * ValuesCanvas.ActualWidth;

                var line = new Line()
                {
                    X1 = ValuesCanvas.ActualWidth - previousPointX,
                    Y1 = ValuesCanvas.ActualHeight - previousPointY,
                    X2 = ValuesCanvas.ActualWidth - currentPointX,
                    Y2 = ValuesCanvas.ActualHeight - currentPointY,
                    Stroke = Brushes.Blue,
                };

                Canvas.SetZIndex(line, 2);
                ValuesCanvas.Children.Add(line);
            }
        }

        private void GenerateLinesForMaxValues()
        {
            for (int i = 1; i < MaxValues.Count; i++)
            {
                var previousPoint = MaxValues[i - 1];
                var currentPoint = MaxValues[i];

                var previousPointY = ((double)previousPoint.Y / (double)MaxYAxisValue) * ValuesCanvas.ActualHeight;
                var currentPointY = ((double)currentPoint.Y / (double)MaxYAxisValue) * ValuesCanvas.ActualHeight;
                var minutesRangePreviousPoint = (ToDate - FromDate).TotalMinutes;
                var minutesValuePreviousPoint = (ToDate - previousPoint.X).TotalMinutes;
                var previousPointX = (minutesValuePreviousPoint / minutesRangePreviousPoint) * ValuesCanvas.ActualWidth;
                var minutesRangeCurrentPoint = (ToDate - FromDate).TotalMinutes;
                var minutesValueCurrentPoint = (ToDate - currentPoint.X).TotalMinutes;
                var currentPointX = (minutesValueCurrentPoint / minutesRangeCurrentPoint) * ValuesCanvas.ActualWidth;

                var line = new Line()
                {
                    X1 = ValuesCanvas.ActualWidth - previousPointX,
                    Y1 = ValuesCanvas.ActualHeight - previousPointY,
                    X2 = ValuesCanvas.ActualWidth - currentPointX,
                    Y2 = ValuesCanvas.ActualHeight - currentPointY,
                    Stroke = Brushes.Red,
                };

                Canvas.SetZIndex(line, 0);
                ValuesCanvas.Children.Add(line);
            }
        }

        private void GenerateLinesForAvegareValues()
        {
            for (int i = 1; i < AvegareValues.Count; i++)
            {
                var previousPoint = AvegareValues[i - 1];
                var currentPoint = AvegareValues[i];

                var previousPointY = ((double)previousPoint.Y / (double)MaxYAxisValue) * ValuesCanvas.ActualHeight;
                var currentPointY = ((double)currentPoint.Y / (double)MaxYAxisValue) * ValuesCanvas.ActualHeight;
                var minutesRangePreviousPoint = (ToDate - FromDate).TotalMinutes;
                var minutesValuePreviousPoint = (ToDate - previousPoint.X).TotalMinutes;
                var previousPointX = (minutesValuePreviousPoint / minutesRangePreviousPoint) * ValuesCanvas.ActualWidth;
                var minutesRangeCurrentPoint = (ToDate - FromDate).TotalMinutes;
                var minutesValueCurrentPoint = (ToDate - currentPoint.X).TotalMinutes;
                var currentPointX = (minutesValueCurrentPoint / minutesRangeCurrentPoint) * ValuesCanvas.ActualWidth;

                var line = new Line()
                {
                    X1 = ValuesCanvas.ActualWidth - previousPointX,
                    Y1 = ValuesCanvas.ActualHeight - previousPointY,
                    X2 = ValuesCanvas.ActualWidth - currentPointX,
                    Y2 = ValuesCanvas.ActualHeight - currentPointY,
                    Stroke = Brushes.Black,
                };

                Canvas.SetZIndex(line, 1);
                ValuesCanvas.Children.Add(line);
            }
        }

        private void ChartPointButton_Click(object sender, RoutedEventArgs e)
        {
            // In future open site with Chart Point details
        }

        private void SwitchTo60DaysModeButton_Click(object sender, RoutedEventArgs e)
        {
            Mode = "60 Days";
            ToDate = DateTime.Now;
            FromDate = ToDate.AddDays(-60);

            var priceChartCollection = PriceChartRepository.GetPriceProductChart(ProductName, FromDate, ToDate);
            AvegareValues = priceChartCollection.AvegarePoints;
            MaxValues = priceChartCollection.MaxPoints;
            MinValues = priceChartCollection.MinPoints;
            if (priceChartCollection.MaxPoints.Count > 0)
            {
                MaxYAxisValue = (int)(priceChartCollection.MaxPoints.Select(p => p.Y).Max() * 1.1);
            }

            GenerateXAxisLabelsFor60Days();
            GenerateYAxisLabels();
            CreateDateRangeName();
            CalculatePointPosition();
        }

        private void SwitchTo30DaysModeButton_Click(object sender, RoutedEventArgs e)
        {
            Mode = "30 Days";
            ToDate = DateTime.Now;
            FromDate = ToDate.AddDays(-30);

            var priceChartCollection = PriceChartRepository.GetPriceProductChart(ProductName, FromDate, ToDate);
            AvegareValues = priceChartCollection.AvegarePoints;
            MaxValues = priceChartCollection.MaxPoints;
            MinValues = priceChartCollection.MinPoints;
            if (priceChartCollection.MaxPoints.Count > 0)
            {
                MaxYAxisValue = (int)(priceChartCollection.MaxPoints.Select(p => p.Y).Max() * 1.1);
            }

            GenerateXAxisLabelsFor30Days();
            GenerateYAxisLabels();
            CreateDateRangeName();
            CalculatePointPosition();
        }

        private void SwitchTo7DaysModeButton_Click(object sender, RoutedEventArgs e)
        {
            Mode = "7 Days";
            ToDate = DateTime.Now;
            FromDate = ToDate.AddDays(-7);

            var priceChartCollection = PriceChartRepository.GetPriceProductChart(ProductName, FromDate, ToDate);
            AvegareValues = priceChartCollection.AvegarePoints;
            MaxValues = priceChartCollection.MaxPoints;
            MinValues = priceChartCollection.MinPoints;
            if (priceChartCollection.MaxPoints.Count > 0)
            {
                MaxYAxisValue = (int)(priceChartCollection.MaxPoints.Select(p => p.Y).Max() * 1.1);
            }

            GenerateXAxisLabelsFor7Days();
            GenerateYAxisLabels();
            CreateDateRangeName();
            CalculatePointPosition();
        }

        private void SwitchTo1DayModeButton_Click(object sender, RoutedEventArgs e)
        {
            Mode = "1 Day";
            ToDate = DateTime.Now;
            FromDate = ToDate.AddDays(-1);

            var priceChartCollection = PriceChartRepository.GetPriceProductChart(ProductName, FromDate, ToDate);
            AvegareValues = priceChartCollection.AvegarePoints;
            MaxValues = priceChartCollection.MaxPoints;
            MinValues = priceChartCollection.MinPoints;
            if (priceChartCollection.MaxPoints.Count > 0)
            {
                MaxYAxisValue = (int)(priceChartCollection.MaxPoints.Select(p => p.Y).Max() * 1.1);
            }

            GenerateXAxisLabelsFor1Day();
            GenerateYAxisLabels();
            CreateDateRangeName();
            CalculatePointPosition();
        }

        private void ValuesHelpLinesGrid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            CalculatePointPosition();
        }

        public void Refresh()
        {
            switch (Mode)
            {
                case "60 Days": SwitchTo60DaysModeButton_Click(null, new RoutedEventArgs()); break;
                case "30 Days": SwitchTo30DaysModeButton_Click(null, new RoutedEventArgs()); break;
                case "7 Days": SwitchTo7DaysModeButton_Click(null, new RoutedEventArgs()); break;
                case "1 Day": SwitchTo1DayModeButton_Click(null, new RoutedEventArgs()); break;
                default: throw new Exception("Incorrect mode.");
            }
        }
    }
}
