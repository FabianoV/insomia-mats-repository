﻿using L2MatsAPI.EntityFramework;
using L2MatsAPI.Models;
using L2MatsAPI.Repositiories.Contract;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Repositiories
{
    public class PriceLogRepository : IPriceLogRepository
    {
        // Properties
        public CustomDbContext DbContext { get; set; }

        // Constructor
        public PriceLogRepository(CustomDbContext dbContext)
        {
            DbContext = dbContext;
        }

        // Methods
        public PriceLog Get(Guid id)
        {
            return DbContext.PriceLogs.FirstOrDefault(pl => pl.Id == id);
        }

        public List<PriceLog> Get(string productName)
        {
            return DbContext.PriceLogs.Where(pl => pl.ProductName == productName).ToList();
        }

        public List<PriceLog> GetFromDay(string productName, DateTime day)
        {
            return DbContext.PriceLogs.Where(pl => pl.ProductName == productName && day.Date.Equals(pl.Time.Date)).ToList();
        }

        public void Insert(PriceLog log)
        {
            // Verify if product for price log exists
            if (log.ProductId != Guid.Empty)
            {
                var product = DbContext.Products.FirstOrDefault(p => p.Id == log.ProductId);
                if (product == null)
                {
                    throw new Exception($"Not found product with id {log.ProductId} in database."); // Data Consistency Exception
                }
                log.ProductName = product.Name;

            }
            else
            {
                var product = DbContext.Products.FirstOrDefault(p => p.Name == log.ProductName);
                if (product == null)
                {
                    throw new Exception($"Not found product with id {log.ProductId} and name {log.ProductName} in database."); // Data Consistency Exception
                }
                log.ProductId = product.Id;
            }

            DbContext.PriceLogs.Add(log);
            DbContext.SaveChanges();
        }

        public void Remove(PriceLog priceLog)
        {
            DbContext.Remove(priceLog);
            DbContext.SaveChanges();
        }
    }
}
