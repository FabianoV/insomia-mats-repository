﻿using L2MatsAPI.EntityFramework;
using L2MatsAPI.Models;
using L2MatsAPI.Models.Enums;
using L2MatsAPI.Repositiories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Repositiories
{
    public class ProductRepository : IProductRepository
    {
        // Properties
        public CustomDbContext DbContext { get; set; }

        // Constructor
        public ProductRepository(CustomDbContext dbContext)
        {
            DbContext = dbContext;
        }

        // Methods
        public List<Product> Get()
        {
            return DbContext.Products.ToList();
        }

        public Product Get(string productName)
        {
            return DbContext.Products.FirstOrDefault(p => p.Name == productName);
        }

        public List<Product> GetGroup(ProductGroupName group)
        {
            return DbContext.Products.Where(p => p.Group == group).ToList();
        }

        public void Insert(Product p)
        {
            if (string.IsNullOrWhiteSpace(p.Name))
            {
                throw new ArgumentException("Product name cannot be null.");
            }

            if (string.IsNullOrWhiteSpace(p.Image))
            {
                throw new ArgumentException("Product should have provided image.");
            }

            DbContext.Products.Add(p);
            DbContext.SaveChanges();
        }
    }
}
