﻿using L2MatsAPI.Authentication;
using L2MatsAPI.EntityFramework;
using L2MatsAPI.Models;
using L2MatsAPI.Repositiories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Repositiories.Implementation
{
    public class UserRepository : IUserRepository
    {
        // Properties
        public CustomDbContext DbContext { get; set; }

        // Construction
        public UserRepository(CustomDbContext dbContext)
        {
            DbContext = dbContext;
        }

        // Methods
        public async Task<User> Add(User user)
        {
            DbContext.Users.Add(user);
            DbContext.SaveChanges();

            return user.WithoutPassword();
        }

        public async Task<User> Authenticate(string username, string password)
        {
            var user = await Task.Run(() => DbContext.Users.FirstOrDefault(u => u.Username == username && u.Password == password));

            // return null if user not found
            if (user == null)
            {
                return null;
            }

            // authentication successful so return user details without password
            return user.WithoutPassword();
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await Task.Run(() => DbContext.Users.WithoutPasswords());
        }
    }
}
