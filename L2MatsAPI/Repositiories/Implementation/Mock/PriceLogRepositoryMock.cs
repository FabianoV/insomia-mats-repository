﻿using L2MatsAPI.Models;
using L2MatsAPI.Models.Enums;
using L2MatsAPI.Repositiories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Repositiories
{
    public class PriceLogRepositoryMock : IPriceLogRepository
    {
        // Properties
        public List<PriceLog> PriceLogs { get; set; }

        // Constructor
        public PriceLogRepositoryMock()
        {
            PriceLogs = new List<PriceLog>();
        }

        // Methods
        public PriceLog Get(Guid id)
        {
            return PriceLogs.FirstOrDefault(pl => pl.Id == id);
        }


        public List<PriceLog> Get(string productName)
        {
            return PriceLogs.Where(pl => pl.ProductName == productName).ToList();
        }

        public List<PriceLog> GetFromDay(string productName, DateTime day)
        {
            return PriceLogs.Where(pl => pl.ProductName == productName && day.Date.Equals(pl.Time.Date)).ToList();
        }

        public void Insert(PriceLog log)
        {
            PriceLogs.Add(log);
        }

        public void Remove(PriceLog priceLog)
        {
            PriceLogs.Remove(priceLog);
        }
    }
}
