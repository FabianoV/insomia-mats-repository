﻿using L2MatsAPI.Models;
using L2MatsAPI.Models.Enums;
using L2MatsAPI.Repositiories.Contract;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Repositiories
{
    public class ProductGroupRepositoryMock : IProductGroupRepository
    {
        // Properties
        private List<ProductGroup> Groups { get; set; }

        // Constructor
        public ProductGroupRepositoryMock()
        {
            Groups = new List<ProductGroup>();

            Groups.Add(new ProductGroup()
            {
                Id = Guid.NewGuid(),
                Group = ProductGroupName.Basic,
                IconUrl = "https://lineage.pmfun.com/data/img/etc_ticket_i00_0.png",
                Name = ProductGroupName.Basic.ToString()
            });

            Groups.Add(new ProductGroup()
            {
                Id = Guid.NewGuid(),
                Group = ProductGroupName.Complex,
                IconUrl = "https://lineage.pmfun.com/data/img/etc_ticket_i00_0.png",
                Name = ProductGroupName.Complex.ToString()
            });

            Groups.Add(new ProductGroup()
            {
                Id = Guid.NewGuid(),
                Group = ProductGroupName.Mold,
                IconUrl = "https://lineage.pmfun.com/data/img/etc_ticket_i00_0.png",
                Name = ProductGroupName.Mold.ToString()
            });
        }

        // Methods
        public List<ProductGroup> GetAll()
        {
            return Groups.ToList();
        }
    }
}
