﻿using L2MatsAPI.Models;
using L2MatsAPI.Models.Enums;
using L2MatsAPI.Products;
using L2MatsAPI.Repositiories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Repositiories
{
    public class ProductRepositoryMock : IProductRepository
    {
        // Properties
        private List<Product> Products { get; set; }

        // Constructor
        public ProductRepositoryMock()
        {
            var allProducts = new ProductsCollection();
            Products = allProducts.All;
        }

        // Methods
        public List<Product> Get()
        {
            return Products.ToList();
        }

        public Product Get(string productName)
        {
            return Products.FirstOrDefault(p => p.Name == productName);
        }

        public List<Product> GetGroup(ProductGroupName group)
        {
            return Products.Where(p => p.Group == group).ToList();
        }

        public void Insert(Product p)
        {
            if (string.IsNullOrWhiteSpace(p.Name))
            {
                throw new ArgumentException("Product name cannot be null.");
            }

            if (string.IsNullOrWhiteSpace(p.Image))
            {
                throw new ArgumentException("Product should have provided image.");
            }

            Products.Add(p);
        }
    }
}
