﻿using L2MatsAPI.EntityFramework;
using L2MatsAPI.Models;
using L2MatsAPI.Repositiories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Repositiories
{
    public class ProductGroupRepository : IProductGroupRepository
    {
        // Properties
        public CustomDbContext DbContext { get; set; }

        // Constructor
        public ProductGroupRepository(CustomDbContext dbContext)
        {
            DbContext = dbContext;
        }

        // Methods
        public List<ProductGroup> GetAll()
        {
            return DbContext.ProductsGroups.ToList();
        }
    }
}
