﻿using L2MatsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Repositiories.Contract
{
    public interface IPriceLogRepository
    {
        PriceLog Get(Guid id);
        List<PriceLog> Get(string productName);
        List<PriceLog> GetFromDay(string productName, DateTime day);
        void Insert(PriceLog log);
        void Remove(PriceLog priceLog);
    }
}
