﻿using L2MatsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Repositiories.Contract
{
    public interface IProductGroupRepository
    {
        List<ProductGroup> GetAll();
    }
}
