﻿using L2MatsAPI.Models;
using L2MatsAPI.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Repositiories.Contract
{
    public interface IProductRepository
    {
        List<Product> Get();
        Product Get(string productName);
        List<Product> GetGroup(ProductGroupName group);
        void Insert(Product p);
    }
}
