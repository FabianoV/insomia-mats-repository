﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Models.Enums
{
    public enum ProductGroupName
    {
        Basic,
        Complex,
        Mold,
    }
}
