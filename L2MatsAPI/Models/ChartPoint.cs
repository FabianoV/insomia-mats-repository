﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Models
{
    public class ChartPoint
    {
        public DateTime X { get; set; }
        public int Y { get; set; }
    }
}
