﻿using L2MatsAPI.Models;
using L2MatsAPI.Repositiories.Contract;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductsGroupController
    {
        // Properties
        public IProductGroupRepository ProductGroups { get; set; }
        public IProductRepository ProductRepository { get; set; }

        // Constructor
        public ProductsGroupController(IProductGroupRepository productGroupRepository, IProductRepository productRepository)
        {
            ProductGroups = productGroupRepository;
            ProductRepository = productRepository;
        }

        // Methods
        [HttpGet]
        [Route("GetAll")]
        public List<ProductGroup> GetAll(bool includeProducts = true)
        {
            var groups = ProductGroups.GetAll();

            if (includeProducts)
            {
                foreach (var group in groups)
                {
                    group.Products = ProductRepository.GetGroup(group.Group);
                }
            }

            return groups;
        }
    }
}
