﻿using L2MatsAPI.Models;
using L2MatsAPI.Repositiories.Contract;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PriceProductChartController
    {
        // Repository
        public IPriceLogRepository PriceLogRepository { get; set; }

        // Constructor
        public PriceProductChartController(IPriceLogRepository priceLogRepository)
        {
            PriceLogRepository = priceLogRepository;
        }

        // Methods
        [HttpGet]
        [Route("GetLastMonthData")]
        public ChartCollection Get(string productName, DateTime fromDate, DateTime toDate)
        {
            // Calculate average data for each day for last 30 days.
            var result = new ChartCollection()
            {
                AvegarePoints = new List<ChartPoint>(),
                MaxPoints = new List<ChartPoint>(),
                MinPoints = new List<ChartPoint>(),
            };

            var daysCount = (toDate - fromDate).Days;

            foreach (var day in Enumerable.Range(0, daysCount))
            {
                var currentDay = fromDate.AddDays(day);
                var logsFromDay = PriceLogRepository.GetFromDay(productName, currentDay);
                logsFromDay = logsFromDay.OrderBy(l => l.Time).ToList();

                if (logsFromDay.Any())
                {
                    var avgPoint = new ChartPoint()
                    {
                        X = currentDay,
                        Y = Convert.ToInt32(logsFromDay.Select(l => l.Price).Average())
                    };
                    result.AvegarePoints.Add(avgPoint);

                    var minPoint = new ChartPoint()
                    {
                        X = currentDay,
                        Y = Convert.ToInt32(logsFromDay.Select(l => l.Price).Min())
                    };
                    result.MinPoints.Add(minPoint);

                    var maxPoint = new ChartPoint()
                    {
                        X = currentDay,
                        Y = Convert.ToInt32(logsFromDay.Select(l => l.Price).Max())
                    };
                    result.MaxPoints.Add(maxPoint);
                }
            }

            return result;
        }
    }
}
