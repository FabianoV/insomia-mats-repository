﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ThumbnailController : ControllerBase
    {
        // Properties
        public IWebHostEnvironment HostEnvironment { get; set; }

        // Constructor
        public ThumbnailController(IWebHostEnvironment hostEnvironment)
        {
            HostEnvironment = hostEnvironment;
        }

        // Methods
        [HttpGet]
        [Route("get")]
        public IActionResult Get(string name)
        {
            var path = HostEnvironment.ContentRootPath + @$"\Content\Thumbnail\{name}.png";
            return new PhysicalFileResult(path, "image/jpeg");
        }
    }
}
