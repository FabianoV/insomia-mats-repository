﻿using L2MatsAPI.Models;
using L2MatsAPI.Repositiories;
using L2MatsAPI.Repositiories.Contract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PriceLogController
    {
        // Properties
        public IPriceLogRepository Logs { get; set; }

        // Constructor
        public PriceLogController(IPriceLogRepository priceLogRepository)
        {
            Logs = priceLogRepository;
        }

        // Methods
        [HttpGet]
        [Route("Get")]
        public List<PriceLog> Get(string productName)
        {
            if (string.IsNullOrWhiteSpace(productName))
            {
                return null;
            }

            return Logs.Get(productName);
        }

        [HttpPost]
        [Route("Add")]
        public void Add(PriceLog log)
        {
            Logs.Insert(log);
        }

        [HttpDelete]
        [Route("Remove")]
        public void Remove(Guid logId)
        {
            var log = Logs.Get(logId);
            Logs.Remove(log);
        }
    }
}
