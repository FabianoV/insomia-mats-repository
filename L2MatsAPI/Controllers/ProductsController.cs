﻿using L2MatsAPI.Models;
using L2MatsAPI.Models.Enums;
using L2MatsAPI.Repositiories.Contract;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductsController
    {
        // Properties
        public IProductRepository Products { get; set; }

        // Constructor
        public ProductsController(IProductRepository productRepository)
        {
            Products = productRepository;
        }

        // Methods
        [HttpGet]
        [Route("GetAll")]
        public IEnumerable<Product> GetAll()
        {
            return Products.Get();
        }

        [HttpGet]
        [Route("GetByGroup")]
        public IEnumerable<Product> GetByGroup(ProductGroupName group)
        {
            return Products.GetGroup(group);
        }

        [HttpGet]
        [Route("GetOne")]
        public Product GetOne(string productName)
        {
            return Products.Get(productName);
        }
    }
}
