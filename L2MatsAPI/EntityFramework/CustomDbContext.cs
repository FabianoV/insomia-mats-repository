﻿using L2MatsAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.EntityFramework
{
    public class CustomDbContext : DbContext
    {
        // Properties
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductGroup> ProductsGroups { get; set; }
        public DbSet<PriceLog> PriceLogs { get; set; }
        public DbSet<User> Users { get; set; }

        // Constructor
        public CustomDbContext(DbContextOptions<CustomDbContext> options) 
            : base(options)
        {
            Database.Migrate();
        }
    }
}
