﻿using L2MatsAPI.Models;
using L2MatsAPI.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Products
{
    public class MoldProducts
    {
        public List<Product> Products { get; set; }

        public MoldProducts()
        {
            Products = new List<Product>();

            AddProduct("Craftsman Mold", "https://lineage.pmfun.com/data/img/etc_mold_i00.png");
            AddProduct("Warsmith's Holder", "https://lineage.pmfun.com/data/img/etc_blacksmiths_frame_i00.png");
            AddProduct("Arcsmith's Anvil", "https://lineage.pmfun.com/data/img/etc_blacksmiths_frame_i00.png");
            AddProduct("Warsmith's Mold", "https://lineage.pmfun.com/data/img/etc_blacksmiths_frame_i00.png");
            AddProduct("Artisan's Frame", "https://lineage.pmfun.com/data/img/etc_artisans_frame_i00.png");
            AddProduct("Leolin's Mold", "https://lineage.pmfun.com/data/img/etc_blacksmiths_frame_i00.png");
            AddProduct("Maestro Anvil Lock", "https://lineage.pmfun.com/data/img/etc_lump_dark_gray_i00.png");
            AddProduct("Maestro Mold", "https://lineage.pmfun.com/data/img/etc_silver_mold_i00.png");
            AddProduct("Maestro Holder", "https://lineage.pmfun.com/data/img/etc_lump_yellow_i00.png");
            AddProduct("Steel Mold", "https://lineage.pmfun.com/data/img/etc_mold_i00.png");
            AddProduct("Blacksmith's Frame", "https://lineage.pmfun.com/data/img/etc_blacksmiths_frame_i00.png");
            AddProduct("Silver Mold", "https://lineage.pmfun.com/data/img/etc_silver_mold_i00.png");
        }

        private void AddProduct(string name, string image)
        {
            Products.Add(new Product()
            {
                Name = name,
                Image = image,
                Group = ProductGroupName.Mold,
            });
        }
    }
}
