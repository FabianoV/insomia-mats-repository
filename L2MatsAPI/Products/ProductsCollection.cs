﻿using L2MatsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Products
{
    public class ProductsCollection
    {
        // Properties
        public List<Product> All { get; set; }
        public List<Product> Basic { get; set; }
        public List<Product> Complex { get; set; }
        public List<Product> Mold { get; set; }

        // Constructor
        public ProductsCollection()
        {
            All = new List<Product>();

            var basicProducts = new BasicProducts();
            var complexProducts = new ComplexProducts();
            var moldProducts = new MoldProducts();

            Basic = basicProducts.Products;
            Complex = complexProducts.Products;
            Mold = moldProducts.Products;

            All.AddRange(basicProducts.Products);
            All.AddRange(complexProducts.Products);
            All.AddRange(moldProducts.Products);
        }
    }
}
