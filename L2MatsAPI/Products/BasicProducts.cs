﻿using L2MatsAPI.Models;
using L2MatsAPI.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Products
{
    public class BasicProducts
    {
        public List<Product> Products { get; set; }

        public BasicProducts()
        {
            Products = new List<Product>();

            AddProduct("Stem", "https://lineage.pmfun.com/data/img/etc_branch_gold_i00_0.png");
            AddProduct("Varnish", "https://lineage.pmfun.com/data/img/etc_pouch_yellow_i00_0.png");
            AddProduct("Suede", "https://lineage.pmfun.com/data/img/etc_suede_i00_0.png");
            AddProduct("Animal Bone", "https://lineage.pmfun.com/data/img/etc_piece_bone_white_i00_0.png");
            AddProduct("Animal Skin", "https://lineage.pmfun.com/data/img/etc_leather_brown_i00_0.png");
            AddProduct("Thread", "https://lineage.pmfun.com/data/img/etc_skein_white_i00_0.png");
            AddProduct("Iron Ore", "https://lineage.pmfun.com/data/img/etc_lump_gray_i00_0.png");
            AddProduct("Coal", "https://lineage.pmfun.com/data/img/etc_coal_i00_0.png");
            AddProduct("Charcoal", "https://lineage.pmfun.com/data/img/etc_charcoal_i00_0.png");
            AddProduct("Silver Nugget", "https://lineage.pmfun.com/data/img/etc_silver_i00_0.png");
            AddProduct("Adamantite Nugget", "https://lineage.pmfun.com/data/img/etc_adamantite_i00_0.png");
            AddProduct("Asofe", "https://lineage.pmfun.com/data/img/etc_gem_blue_i00_0.png");
            AddProduct("Thons", "https://lineage.pmfun.com/data/img/etc_gem_clear_i00_0.png");
            AddProduct("Mold Glue", "https://lineage.pmfun.com/data/img/etc_reagent_red_i00_0.png");
            AddProduct("Mold Lubricant", "https://lineage.pmfun.com/data/img/etc_reagent_gold_i00_0.png");
            AddProduct("Mold Hardener", "https://lineage.pmfun.com/data/img/etc_reagent_blue_i00_0.png");
            AddProduct("Enria", "https://lineage.pmfun.com/data/img/etc_gem_red_i00_0.png");
            AddProduct("Oriharukon Ore", "https://lineage.pmfun.com/data/img/etc_oriharukon_ore_i00_0.png");
            AddProduct("Mithril Ore", "https://lineage.pmfun.com/data/img/etc_mithril_ore_i00_0.png");
            AddProduct("Braided Hemp", "https://lineage.pmfun.com/data/img/etc_braided_hemp_i00_0.png");
            AddProduct("Metal Hardener", "https://lineage.pmfun.com/data/img/etc_plate_blue_i00.png");
        }

        private void AddProduct(string name, string image)
        {
            Products.Add(new Product()
            {
                Name = name,
                Image = image,
                LocalImage = $"https://localhost:44317/Thumbnail/get?name={name}",
                Group = ProductGroupName.Basic
            });
        }
    }
}
