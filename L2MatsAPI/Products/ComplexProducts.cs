﻿using L2MatsAPI.Models;
using L2MatsAPI.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L2MatsAPI.Products
{
    public class ComplexProducts
    {
        public List<Product> Products { get; set; }

        public ComplexProducts()
        {
            Products = new List<Product>();

            AddProduct("Durable Metal Plate", "https://lineage.pmfun.com/data/img/etc_squares_wood_i00.png");
            AddProduct("Coarse Bone Powder", "https://lineage.pmfun.com/data/img/etc_powder_white_i00.png");
            AddProduct("Steel", "https://lineage.pmfun.com/data/img/etc_lump_black_i00.png");
            AddProduct("Leather", "https://lineage.pmfun.com/data/img/etc_leather_i00.png");
            AddProduct("Cokes", "https://lineage.pmfun.com/data/img/etc_oil_pot_black_i00.png");
            AddProduct("Compound Braid", "https://lineage.pmfun.com/data/img/etc_compound_braid_i00.png");
            AddProduct("Synthetic Cokes", "https://lineage.pmfun.com/data/img/etc_synthetic_cokes_i00.png");
            AddProduct("Varnish of Purity", "https://lineage.pmfun.com/data/img/etc_potion_clear_i00.png");
            AddProduct("Cord", "https://lineage.pmfun.com/data/img/etc_skein_gray_i00.png");
            AddProduct("High Grade Suede", "https://lineage.pmfun.com/data/img/etc_pouch_yellow_i00.png");
            AddProduct("Metallic Fiber", "https://lineage.pmfun.com/data/img/etc_metallic_fiber_i00.png");
            AddProduct("Oriharukon", "https://lineage.pmfun.com/data/img/etc_oriharukon_i00.png");
            AddProduct("Metallic Thread", "https://lineage.pmfun.com/data/img/etc_skein_gray_i00.png");
            AddProduct("Mithril Alloy", "https://lineage.pmfun.com/data/img/etc_lump_white_i00.png");
            AddProduct("Crafted Leather", "https://lineage.pmfun.com/data/img/etc_crafted_leather_i00.png");
        }

        private void AddProduct(string name, string image)
        {
            Products.Add(new Product()
            {
                Name = name,
                Image = image,
                Group = ProductGroupName.Complex,
            });
        }
    }
}
