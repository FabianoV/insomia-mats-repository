﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace L2MatsAPI.Migrations
{
    public partial class M1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PriceLogs_Products_ProductId",
                table: "PriceLogs");

            migrationBuilder.DropIndex(
                name: "IX_PriceLogs_ProductId",
                table: "PriceLogs");

            migrationBuilder.AlterColumn<Guid>(
                name: "ProductId",
                table: "PriceLogs",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProductName",
                table: "PriceLogs",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProductName",
                table: "PriceLogs");

            migrationBuilder.AlterColumn<Guid>(
                name: "ProductId",
                table: "PriceLogs",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.CreateIndex(
                name: "IX_PriceLogs_ProductId",
                table: "PriceLogs",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_PriceLogs_Products_ProductId",
                table: "PriceLogs",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
