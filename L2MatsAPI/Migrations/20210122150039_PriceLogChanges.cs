﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace L2MatsAPI.Migrations
{
    public partial class PriceLogChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Product",
                table: "PriceLogs");

            migrationBuilder.AddColumn<Guid>(
                name: "ProductId",
                table: "PriceLogs",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PriceLogs_ProductId",
                table: "PriceLogs",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_PriceLogs_Products_ProductId",
                table: "PriceLogs",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PriceLogs_Products_ProductId",
                table: "PriceLogs");

            migrationBuilder.DropIndex(
                name: "IX_PriceLogs_ProductId",
                table: "PriceLogs");

            migrationBuilder.DropColumn(
                name: "ProductId",
                table: "PriceLogs");

            migrationBuilder.AddColumn<string>(
                name: "Product",
                table: "PriceLogs",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
